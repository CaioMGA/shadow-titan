#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <stdbool.h>
#include <stdio.h>
#include <conio.h>

//constantes, structs e enums
int SCREEN_WIDTH = 1024;
int SCREEN_HEIGHT = 576;
int TILE_SIZE = 32;
int TILES_HORIZONTAL = 32;
int TILES_VERTICAL   = 18;
const int FRAME_TIME = 4;
const int FRAMES_INVENCIVEL = 30;

const int JOYSTICK_DEAD_ZONE = 10000;

int TOTAL_PLAYERS;
int TOTAL_FLECHAS;

enum status {
    OK,
    FAIL,
    QUIT
};

enum animationState{
    IDLE,
    WALKING,
    DASHING,
    AIMING

};

enum directions{ // sentido horario
    UP,
    UPRIGHT,
    RIGHT,
    DOWNRIGHT,
    DOWN,
    DOWNLEFT,
    LEFT,
    UPLEFT,
    NONE
};

enum collisionGroups{
    HEROI,
    ENEMY,
    FLECHA,
    WALL
};

enum menu{
    TITLE_SCREEN,
    MAIN_MENU,
    PRE_GAME,
    GAME,
    PRE_ROUND,
    ROUND,
    POS_ROUND,
    GAMEOVER,
    CREDITS,
    SETTINGS
};

typedef struct{
    SDL_Rect rect;
    SDL_Rect crop;
    SDL_Texture * sprite;
    float velocidade;
    int dano;
    int direcao;
    int dirX;
    int dirY;
    int tempoDeVoo;
    int collisionGroup;
    bool voando;
    bool canBePicked;

}
Flecha;

typedef struct{
    int life;
    int direcao;
    int dirX;
    int dirY;
    int state;
    int animationFrame;
    int frameTime;
    int invencivelTimer;
    int collisionGroup;
    int pontuacao;
    float velocidade;
    SDL_Rect rect;
    SDL_Rect crop;
    SDL_Texture * sprite;
    Flecha * flecha;
    Flecha * flechaLancada;
    int timerLancamento;

}
Personagem;

typedef struct{

} Muro;
//Escopo das fun��es

void aplicaDano(Personagem *, int);
int checkEvents();
void close();
bool colisao(SDL_Rect *, SDL_Rect *);
void createFlechas();
void createHerois();
void createCenario();
void dispara(Personagem *);
void draw();
void drawFlechas();
void drawLifeBars();
void drawPlayers();
void drawPlacar();
Flecha initFlecha(char *, int, int, int);
Personagem initHeroi(char *, int, int, int);
int loadAssets();
SDL_Texture * loadTexture(char *);
void moveFlechas();
void moveHerois();
void novoRound();
void resolveDirecaoHerois();
int start();
void startLifeBars();
void startJoystick();
void startPlacar();
void update();
void VerificaPontuacao();
// variaveis globais
SDL_Window * window = NULL;
SDL_Renderer * renderer = NULL;
SDL_Renderer * bgRenderer = NULL;
SDL_Surface * screenSurface = NULL;
SDL_Texture * bg = NULL;
SDL_Texture * collisionMask = NULL;
SDL_Texture * cenario = NULL;
SDL_Texture * lifeBar = NULL;
SDL_Texture * muro = NULL;
SDL_Rect bgRect;
SDL_Rect tileRect;
SDL_Event event;
SDL_Rect lifeBarsPositions [2];
SDL_Rect lifeBarsCrops [2];
SDL_Rect score[2];

SDL_Joystick * gameController[2];

TTF_Font * fonte = NULL;
SDL_Texture * textTex;
SDL_Surface * textSurface;

Flecha flechas[4];
Personagem herois[2];

int heroiFrames[8][3][2] = {
    {{64, 96},{32, 96},{0, 96}},        //UP
    {{160, 96},{128, 96},{96, 96}},     //UPRIGHT
    {{64, 64},{32, 64},{0, 64}},        //RIGHT
    {{160, 64},{128, 64},{96, 64}},      //DOWNRIGHT
    {{64, 0},{32, 0},{0, 0}},           //DOWN
    {{160, 0},{128, 0},{96, 0}},       //DOWNLEFT
    {{64, 32},{32, 32},{0, 32}},        //LEFT
    {{160, 32},{128, 32},{98, 32}}      //UPLEFT
};

int cenarioTiled[18][32] = {
    {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 5},
    {5, 1, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 5},
    {5, 1, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8, 8, 8, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 5},
    {5, 1, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 8, 8, 8, 8, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 5},
    {5, 1, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 5},
    {5, 1, 1, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 8, 8, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5},
    {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5} };

int JOYSTICK_BUTTON_B_DOWN = 0;
int GAME_STATE = TITLE_SCREEN;

//fun��es

int start(){
    int resultado = OK;

    if(SDL_Init(SDL_INIT_EVERYTHING)){ // verifica se SDL iniializou
        printf("Falha ao iniciar SDL2. SDL Error: %s\n", SDL_GetError());
        resultado = FAIL;
    } else {
        IMG_Init(IMG_INIT_PNG);
        window = SDL_CreateWindow(
            "Shadow of Titan",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN);
        if(window == NULL){ //verifica se a janela foi criada
            printf("Falha ao iniciar SDL2. SDL Error: %s\n", SDL_GetError());
            resultado = FAIL;
        } else {
            renderer = SDL_CreateRenderer(window, -1, 0);
            if(renderer == NULL){
                printf("Falha ao criar Renderer. SDL Error: %s\n", SDL_GetError());
                resultado = FAIL;
            }
            screenSurface = SDL_GetWindowSurface(window);
            if(screenSurface == NULL){
                printf("Falha ao criar screenSurface. SDL Error: %s\n", SDL_GetError());
                resultado = FAIL;
            }
        }
    }

    createHerois();
    createFlechas();
    bgRect.x = 0;
    bgRect.y = 0;
    bgRect.w = SCREEN_WIDTH;
    bgRect.h = SCREEN_HEIGHT;
    loadAssets();
    startJoystick();
    startLifeBars();
    tileRect.x = 0;
    tileRect.y = 0;
    tileRect.w = TILE_SIZE;
    tileRect.h = TILE_SIZE;

    createCenario();
    bgRenderer = SDL_CreateRenderer(window, -1, 0);
    startPlacar();

    return resultado;
}

void startJoystick(){
    if(SDL_NumJoysticks() < 1){
        printf("Nenhum Joystick detectado.\n");
    } else {
        gameController[0] = SDL_JoystickOpen(0);
        if(gameController[0] == NULL){
            printf("Nao foi possivel usar o joystick. SDL ERROR: %s\n", SDL_GetError());
        }
    }

}

void startLifeBars(){
    lifeBarsPositions[0].w = 24;
    lifeBarsPositions[0].h = 200;
    lifeBarsPositions[0].x = 4;
    lifeBarsPositions[0].y = (SCREEN_HEIGHT / 2) - (lifeBarsPositions[0].h / 2);

    lifeBarsCrops[0].x = 0;
    lifeBarsCrops[0].y = 0;
    lifeBarsCrops[0].w = 24;
    lifeBarsCrops[0].h = 200;

    if(TOTAL_PLAYERS == 2){
        lifeBarsPositions[1].w = 24;
        lifeBarsPositions[1].h = 200;
        lifeBarsPositions[1].x = SCREEN_WIDTH - lifeBarsPositions[1].w - 4;
        lifeBarsPositions[1].y = (SCREEN_HEIGHT / 2) - (lifeBarsPositions[1].h / 2);

        lifeBarsCrops[1].x = 0;
        lifeBarsCrops[1].y = 0;
        lifeBarsCrops[1].w = 24;
        lifeBarsCrops[1].h = 200;
    }
}
void startPlacar(){

    score[0].w = 24;
    score[0].h = 24;
    score[0].x = 4;
    score[0].y = (SCREEN_HEIGHT / 2) + (lifeBarsPositions[0].h / 2) + 10;

    if(TOTAL_PLAYERS == 2){
        score[1].w = 24;
        score[1].h = 24;
        score[1].x = SCREEN_WIDTH - lifeBarsPositions[1].w - 4;
        score[1].y = (SCREEN_HEIGHT / 2) + (lifeBarsPositions[1].h / 2) + 10;

            }
}
Personagem initHeroi(char * path, int x, int y, int life){
    Personagem heroi;
    heroi.rect.x = x;
    heroi.rect.y = y;
    heroi.rect.w = 64;
    heroi.rect.h = 64;

    heroi.crop.x = 0;
    heroi.crop.y = 0;
    heroi.crop.w = 32;
    heroi.crop.h = 32;

    heroi.life = life;
    heroi.state = IDLE;
    heroi.direcao = UP;
    heroi.dirX = 0;
    heroi.dirY = 0;
    heroi.velocidade = 4;
    heroi.animationFrame = 3;
    heroi.pontuacao = 0;
    heroi.frameTime = FRAME_TIME;
    heroi.flecha = NULL;
    heroi.flechaLancada = NULL;
    heroi.sprite = loadTexture(path);

    return heroi;
}

Flecha initFlecha(char * path, int x, int y, int dano){
    Flecha f;
    f.rect.x = x;
    f.rect.y = y;
    f.rect.w = 32;
    f.rect.h = 32;

    f.crop.x = 0;
    f.crop.y = 0;
    f.crop.w = 16;
    f.crop.h = 16;

    f.dano = dano;
    f.canBePicked = true;
    f.direcao = NONE;
    f.sprite = loadTexture(path);

    return f;
}

void createFlechas(){

    flechas[0] = initFlecha("img/flecha/mini_fogo.png", 200, (SCREEN_HEIGHT / 2) + 8, 1);
    flechas[1] = initFlecha("img/flecha/mini_gelo.png", 840, (SCREEN_HEIGHT / 2) + 8, 1);
    flechas[2] = initFlecha("img/flecha/mini_normal.png", (SCREEN_WIDTH / 2) - 8, 56 - (flechas[2].rect.h / 2), 1);
    flechas[3] = initFlecha("img/flecha/mini_normal.png", (SCREEN_WIDTH / 2) - 8 , (SCREEN_HEIGHT - 72), 1);
    TOTAL_FLECHAS = 4;

}

void createHerois(){
    herois[0] = initHeroi("img/heroi/hero-8dir-chroma-3.png", 64, (SCREEN_HEIGHT / 2)  - 16, 2);
    herois[0].direcao = RIGHT;
    herois[1] = initHeroi("img/heroi/hero-8dir-chroma-5.png", 928, (SCREEN_HEIGHT / 2) - 16, 2);
    herois[1].direcao = LEFT;
    TOTAL_PLAYERS = 2;
}

void dispara(Personagem * heroi){
    if(heroi ->flecha != NULL){

        switch(heroi->direcao){
        case UP:
            heroi->flecha->dirX = 0;
            heroi->flecha->dirY = -1;
        break;
        case UPRIGHT:
            heroi->flecha->dirX = 1;
            heroi->flecha->dirY = -1;
        break;
        case RIGHT:
            heroi->flecha->dirX = 1;
            heroi->flecha->dirY = 0;
        break;
        case DOWNRIGHT:
            heroi->flecha->dirX = 1;
            heroi->flecha->dirY = 1;
        break;
        case DOWN:
            heroi->flecha->dirX = 0;
            heroi->flecha->dirY = 1;
        break;
        case DOWNLEFT:
            heroi->flecha->dirX = -1;
            heroi->flecha->dirY = 1;
        break;
        case LEFT:
            heroi->flecha->dirX = -1;
            heroi->flecha->dirY = 0;
        break;
        case UPLEFT:
            heroi->flecha->dirX = -1;
            heroi->flecha->dirY = -1;
        break;
        }

        heroi->flecha -> voando = true;
        heroi->flecha -> velocidade = 12;
        heroi->flecha -> tempoDeVoo = 60;
        heroi->flechaLancada = heroi->flecha;
        heroi->flecha = NULL;
        heroi->timerLancamento = 10;
    }
}

SDL_Texture * loadTexture(char * path){
    SDL_Surface * loadedSurface = NULL;
    SDL_Surface * finalSurface = NULL;
    SDL_Texture * newTexture = NULL;
    loadedSurface = IMG_Load(path);
    if(loadedSurface == NULL){
        printf("Nao foi possivel carregar a imagem: %s. SDL_image Error: %s", path, IMG_GetError());
    } else {
        SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ));
        finalSurface = SDL_ConvertSurface(loadedSurface, screenSurface -> format, NULL);

        if(finalSurface == NULL){
           printf("Nao foi possivel converter a imagem %s para o formato da tela. SDL_image Error: %s", path, IMG_GetError());
        }
    }
    newTexture = SDL_CreateTextureFromSurface(renderer, finalSurface);
    SDL_FreeSurface(loadedSurface);
    SDL_FreeSurface(finalSurface);

    return newTexture;
}

void createCenario(){
    SDL_Surface * tileset = NULL;
    SDL_Surface * loadedSurface = NULL;
    SDL_Surface * finalSurface = NULL;
    SDL_Renderer * tempCollisionMask = SDL_CreateRenderer(window, -1, 0);
    SDL_Texture * newTexture = NULL;


    SDL_Rect positionOnScreen;
    SDL_Rect selectedTile;

    loadedSurface = SDL_GetWindowSurface(window);
    tileset = IMG_Load("img/outros/tileset.png");

    selectedTile.y = 0;
    selectedTile.w = TILE_SIZE;
    selectedTile.h = TILE_SIZE;

    int i; int j;
    for(i = 0; i < TILES_VERTICAL; i++){
        for(j = 0; j < TILES_HORIZONTAL; j++){
             selectedTile.x = cenarioTiled[i][j] * TILE_SIZE;

            positionOnScreen.x = j * TILE_SIZE;
            positionOnScreen.y = i * TILE_SIZE;
            positionOnScreen.w = TILE_SIZE;
            positionOnScreen.h = TILE_SIZE;

            SDL_BlitSurface(tileset, &selectedTile, loadedSurface, &positionOnScreen);

        }
    }

    finalSurface = SDL_ConvertSurface(loadedSurface, screenSurface -> format, NULL);
    bg = SDL_CreateTextureFromSurface(renderer, finalSurface);
    SDL_FreeSurface(loadedSurface);
    SDL_FreeSurface(finalSurface);
    SDL_FreeSurface(tileset);
}

int loadAssets(){
    lifeBar = loadTexture("img/outros/lifebar.png");
    muro = loadTexture("img/outros/muro.png");
    return OK;
}

void moveHerois(){
    int i;
    for(i = 0; i < TOTAL_PLAYERS; i++){
        if(herois[i].state == WALKING){
            herois[i].rect.x += herois[i].velocidade * herois[i].dirX;
            herois[i].rect.y += herois[i].velocidade * herois[i].dirY;
        }
        if(herois[i].rect.x < 0){
            herois[i].rect.x = 0;
        }
        if(herois[i].rect.y < 0){
            herois[i].rect.y = 0;
        }
        if(herois[i].rect.x > SCREEN_WIDTH - 64){
            herois[i].rect.x = SCREEN_WIDTH - 64;
        }
        if(herois[i].rect.y > SCREEN_HEIGHT - 64){
            herois[i].rect.y = SCREEN_HEIGHT - 64;
        }
        if(herois[i].invencivelTimer > 0){
            herois[i].invencivelTimer--;
        }

        if(herois[i].timerLancamento > 0){
            herois[i].timerLancamento--;
        } else {
            herois[i].flechaLancada = NULL;
        }
    }
}

void moveFlechas(){
    int i;
    for(i = 0; i < TOTAL_FLECHAS; i++){
        if(flechas[i].voando){
           if(flechas[i].tempoDeVoo > 0){
                if(flechas[i].tempoDeVoo < 30){
                    flechas[i].velocidade *= 0.91f;
                }
                flechas[i].tempoDeVoo -= 1;

                flechas[i].rect.x += flechas[i].velocidade * flechas[i].dirX;
                flechas[i].rect.y += flechas[i].velocidade * flechas[i].dirY;

               if(flechas[i].rect.x >= SCREEN_WIDTH - flechas[i].rect.w || flechas[i].rect.x <= 0){
                    flechas[i].dirX *= -1;
               }
               if(flechas[i].rect.y >= SCREEN_HEIGHT - flechas[i].rect.h || flechas[i].rect.y <= 0){
                    flechas[i].dirY *= -1;
               }

           } else {
                flechas[i].voando = false;
                flechas[i].canBePicked = true;
           }
            int ii;
            for(ii = 0; ii < TOTAL_PLAYERS; ii++){
                if(colisao(&herois[ii].rect, &flechas[i].rect)){
                    if(herois[ii].flechaLancada != &flechas[i]){
                        aplicaDano(&herois[ii], flechas[i].dano);
                    }
                }
            }
        }

        int j;
        for(j = 0; j < TOTAL_PLAYERS; j++){
            if(flechas[i].canBePicked && colisao(&flechas[i].rect, &herois[j].rect) && herois[j].flecha == NULL){
                herois[j].flecha = &flechas[i];
                herois[j].flecha->canBePicked = false;
            }
        }
    }
}

void resolveDirecaoHerois(){
    int i;
    for(i = 0; i < TOTAL_PLAYERS; i++){
        if(herois[i].dirY == 1){
            if(herois[i].dirX == 1){
                herois[i].direcao = DOWNRIGHT;
            } else if(herois[i].dirX == -1){
                herois[i].direcao = DOWNLEFT;
            } else {
                herois[i].direcao = DOWN;
            }
        } else if(herois[i].dirY == -1){
            if(herois[i].dirX == 1){
                herois[i].direcao = UPRIGHT;
            } else if(herois[i].dirX == -1){
                herois[i].direcao = UPLEFT;
            } else {
                herois[i].direcao = UP;
            }
        } else {
            if(herois[i].dirX == 1){
                herois[i].direcao = RIGHT;
            } else if(herois[i].dirX == -1){
                herois[i].direcao = LEFT;
            }
        }
    }

}

void aplicaDano(Personagem * heroi, int dano){
    if(heroi->invencivelTimer <= 0){
        heroi->life -= dano;
        heroi->invencivelTimer = FRAMES_INVENCIVEL;
    }
}

bool colisao(SDL_Rect * A, SDL_Rect * B){
    bool resultado = false;
    if(((B->x >= A->x) && (B->x <= A->x + A->w) && (B->y >= A->y) && (B->y <= A->y + A->h)) ||
       ((A->x >= B->x) && (A->x <= B->x + B->w) && (A->y >= B->y) && (A->y <= B->y + B->h))){
            resultado = true;
    }

    return resultado;
}

int checkEvents(){
    while(SDL_PollEvent(&event) != 0){
        if(event.type == SDL_QUIT){
            return QUIT;
        } else if(event.type == SDL_KEYUP){ //player 1 teclado
            Uint8 * teclas = SDL_GetKeyboardState(0);
            if(teclas[SDL_SCANCODE_W] == 0 &&
               teclas[SDL_SCANCODE_S] == 0){
                herois[0].dirY = 0;
            }

            if(teclas[SDL_SCANCODE_A] == 0 &&
               teclas[SDL_SCANCODE_D] == 0){
                herois[0].dirX = 0;
            }


        }
        if(event.type == SDL_KEYDOWN){
            Uint8 * teclas = SDL_GetKeyboardState(0);
            if(teclas[SDL_SCANCODE_SPACE] == 1){
                dispara(&herois[0]);
            }

            if(teclas[SDL_SCANCODE_A] == 1){
                herois[0].dirX = -1;
                herois[0].state = WALKING;
            }
            if(teclas[SDL_SCANCODE_D] == 1){
                herois[0].dirX = 1;
                herois[0].state = WALKING;
            }
            if(teclas[SDL_SCANCODE_W] == 1){
                herois[0].dirY = -1;
                herois[0].state = WALKING;
            }
            if(teclas[SDL_SCANCODE_S] == 1){
                herois[0].dirY = 1;
                herois[0].state = WALKING;
            }

            if(teclas[SDL_SCANCODE_LSHIFT] == 1){
                herois[0].state = AIMING;
            }
        }


        if(event.type == SDL_JOYAXISMOTION){
            if(event.jaxis.which == 0){
                if(event.jaxis.axis == 0){ //eixo horizontal
                    if(event.jaxis.value < -JOYSTICK_DEAD_ZONE){
                        herois[1].dirX = -1;
                        herois[1].state = WALKING;
                    } else if(event.jaxis.value > JOYSTICK_DEAD_ZONE){
                        herois[1].dirX = 1;
                        herois[1].state = WALKING;
                    } else {
                        herois[1].dirX = 0;
                    }
                }
                if(event.jaxis.axis == 1){ //eixo vertical
                    if(event.jaxis.value < -JOYSTICK_DEAD_ZONE){
                        herois[1].dirY = -1;
                        herois[1].state = WALKING;
                    } else if(event.jaxis.value > JOYSTICK_DEAD_ZONE){
                        herois[1].dirY = 1;
                        herois[1].state = WALKING;
                    } else {
                        herois[1].dirY = 0;
                    }
                }
            }
        }
        if(event.type == SDL_JOYBUTTONDOWN){
            if(event.jbutton.button == SDL_CONTROLLER_BUTTON_A){
                dispara(&herois[1]);
            }
        }
        if(event.type == SDL_JOYBUTTONDOWN){
            if(event.jbutton.button == SDL_CONTROLLER_BUTTON_B){
                JOYSTICK_BUTTON_B_DOWN = 1;
            }
        }
        if(event.type == SDL_JOYBUTTONUP){
            if(event.jbutton.button == SDL_CONTROLLER_BUTTON_B){
                JOYSTICK_BUTTON_B_DOWN = 0;
            }
        }
        if(JOYSTICK_BUTTON_B_DOWN == 1){
            herois[1].state = AIMING;
        }

        //Player 1
        if(herois[0].dirX == 0 && herois[0].dirY == 0){
            herois[0].state = IDLE;
        }

        //Player 2
        if(herois[1].dirX == 0 && herois[1].dirY == 0){
            herois[1].state = IDLE;
        }
    }
    return OK;
}

void update(){

    moveHerois();
    moveFlechas();
    VerificaPontuacao();

}

void draw(){
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, bg, &bgRect, &bgRect);
    //drawCenario();
    drawPlayers();
    drawFlechas();
    //desenha inimigo
    drawLifeBars();
    drawPlacar();
    SDL_SetRenderTarget(bgRenderer, NULL);
    SDL_RenderPresent(bgRenderer);

    SDL_SetRenderTarget(renderer, NULL);
    SDL_RenderPresent(renderer);
    SDL_Delay(10);
}

void drawLifeBars(){
    if(herois[0].life > 0){
        SDL_Rect p1LifeBar;
        p1LifeBar.x = lifeBarsPositions[0].x;
        p1LifeBar.y = lifeBarsPositions[0].y + (2 - herois[0].life) * 100;
        p1LifeBar.w = lifeBarsPositions[0].w;
        p1LifeBar.h = herois[0].life * 100;

        SDL_SetRenderDrawColor(renderer, 200, 20, 20, 150);
        SDL_RenderFillRect(renderer, &p1LifeBar);
    }
    SDL_RenderCopy(renderer, lifeBar, &lifeBarsCrops[0], &lifeBarsPositions[0]);

    if(TOTAL_PLAYERS == 2){
        if(herois[1].life > 0){
            SDL_Rect p2LifeBar;
            p2LifeBar.x = lifeBarsPositions[1].x;
            p2LifeBar.y = lifeBarsPositions[1].y + (2 - herois[1].life) * 100;
            p2LifeBar.w = lifeBarsPositions[1].w;
            p2LifeBar.h = herois[1].life * 100;

            SDL_SetRenderDrawColor(renderer, 20, 20, 200, 150);
            SDL_RenderFillRect(renderer, &p2LifeBar);
        }
        SDL_RenderCopy(renderer, lifeBar, &lifeBarsCrops[1], &lifeBarsPositions[1]);

    }
}

void drawPlayers(){
    int i;
    for(i = 0; i < TOTAL_PLAYERS; i++){
        if(herois[i].state == WALKING){
            if(herois[i].frameTime < 0){
            herois[i].animationFrame ++;
            herois[i].frameTime = FRAME_TIME;
            if(herois[i].animationFrame > 2){
                herois[i].animationFrame = 0;
            }
            herois[i].frameTime = FRAME_TIME;
        } else {
            herois[i].frameTime--;
        }

        herois[i].crop.x = heroiFrames[herois[i].direcao][herois[i].animationFrame][0];
        herois[i].crop.y = heroiFrames[herois[i].direcao][herois[i].animationFrame][1];
        } else {
            herois[i].crop.x = heroiFrames[herois[i].direcao][1][0];
            herois[i].crop.y = heroiFrames[herois[i].direcao][1][1];
        }

        // desenha a flecha com o jogador
        if(herois[i].flecha != NULL){
            Flecha * f = herois[i].flecha;
            switch (herois[i].direcao){
            case UP:
                f->rect.x = herois[i].rect.x + 16;
                f->rect.y = herois[i].rect.y - 16;
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                break;
            case UPRIGHT:
                f->rect.x = herois[i].rect.x + 48;
                f->rect.y = herois[i].rect.y;
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                break;
            case RIGHT:
                f->rect.x = herois[i].rect.x + 48;
                f->rect.y = herois[i].rect.y + 16;
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                break;
            case DOWNRIGHT:
                f->rect.x = herois[i].rect.x + 48;
                f->rect.y = herois[i].rect.y + 36;
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                break;
            case DOWN:
                f->rect.x = herois[i].rect.x + 16;
                f->rect.y = herois[i].rect.y + 48;
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                break;
            case DOWNLEFT:
                f->rect.x = herois[i].rect.x + -16;
                f->rect.y = herois[i].rect.y + 32;
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                break;
            case LEFT:
                f->rect.x = herois[i].rect.x - 16;
                f->rect.y = herois[i].rect.y + 16;
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                break;
            case UPLEFT:
                f->rect.x = herois[i].rect.x - 16;
                f->rect.y = herois[i].rect.y;
                SDL_RenderCopy(renderer, f->sprite, &f->crop, &f->rect);
                SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
                break;
            }
        } else {
            SDL_RenderCopy(renderer, herois[i].sprite, &herois[i].crop, &herois[i].rect);
        }
    }
}

void drawFlechas(){
    int i;
    for(i = 0; i < TOTAL_FLECHAS; i++){
        int j;
        for(j = 0; j < TOTAL_PLAYERS; j++){
            if(&flechas[i] != herois[j].flecha){
                SDL_RenderCopy(renderer, flechas[i].sprite, &flechas[i].crop, &flechas[i].rect);
            }
        }
    }
}

void drawPlacar(){
    int i;
    for(i = 0; i < TOTAL_PLAYERS; i++){
        int j;
        for(j = 0; j < herois[i].pontuacao; j++){
        SDL_Rect playerScore;


        playerScore.x = score[i].x;
        playerScore.y = score[i].y + j * TILE_SIZE;
        playerScore.w = score[i].w;
        playerScore.h = score[i].h;


        if(i == 0){
            SDL_SetRenderDrawColor(renderer, 200, 20, 20, 150);
        } else if(i == 1){
            SDL_SetRenderDrawColor(renderer, 20, 20, 200, 150);
        }

        SDL_RenderFillRect(renderer, &playerScore);
        }

    }

}

void novoRound(){

    flechas[0].rect.x = 200;
    flechas[0].rect.y = (SCREEN_HEIGHT / 2) + 8;
    flechas[1].rect.x = 840;
    flechas[1].rect.y = (SCREEN_HEIGHT / 2) + 8;
    flechas[2].rect.x =(SCREEN_WIDTH / 2) - 8;
    flechas[2].rect.y = 56 - (flechas[2].rect.h / 2);
    flechas[3].rect.x = (SCREEN_WIDTH / 2) - 8;
    flechas[3].rect.y = (SCREEN_HEIGHT - 72);

    herois[0].rect.x = 64;
    herois[0].rect.y = (SCREEN_HEIGHT / 2)  - 16;
    herois[0].direcao = RIGHT;
    herois[0].life = 2;

    herois[1].rect.x = 928;
    herois[1].rect.y = (SCREEN_HEIGHT / 2) - 16;
    herois[1].direcao = LEFT;
    herois[1].life = 2;

    int i;
    for(i = 0; i < TOTAL_PLAYERS; i++){
        herois[i].flecha = NULL;
    }

    for(i = 0; i < TOTAL_FLECHAS; i++){
        flechas[i].tempoDeVoo = 0;
        flechas[i].canBePicked = true;
        flechas[i].voando = false;
    }

}

void VerificaPontuacao(){
    if(herois[0].life < 0){
        herois[1].pontuacao += 1;
        novoRound();
    }

    if(herois[1].life < 0){
        herois[0].pontuacao += 1;
        novoRound();
    }

}
void close(){
    //SDL_DestroyWindow(window);
    //SDL_FreeSurface(screenSurface);
    //SDL_FreeSurface(heroi.sprite);
    //SDL_FreeSurface(flechaNormal.sprite);

    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
}

int main(int argc, char * args[]){
    if(GAME_STATE == TITLE_SCREEN){
        //draw title screen
        printf(" .----------------. .----------------. .----------------. .----------------. .----------------. .----------------.\n");
        printf("| .--------------. | .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |\n");
        printf("| |    _______   | | |  ____  ____  | | |      __      | | |  ________    | | |     ____     | | | _____  _____ | |\n");
        printf("| |   /  ___  |  | | | |_   ||   _| | | |     /  \\     | | | |_   ___ `.  | | |   .'    `.   | | ||_   _||_   _|| |\n");
        printf("| |  |  (__ \\_|  | | |   | |__| |   | | |    / /\\ \\    | | |   | |   `. \\ | | |  /  .--.  \\  | | |  | | /\\ | |  | |\n");
        printf("| |   '.___`-.   | | |   |  __  |   | | |   / ____ \\   | | |   | |    | | | | |  | |    | |  | | |  | |/  \\| |  | |\n");
        printf("| |  |`\\____) |  | | |  _| |  | |_  | | | _/ /    \\ \\_ | | |  _| |___.' / | | |  \\  `--'  /  | | |  |   /\\   |  | |\n");
        printf("| |  |_______.'  | | | |____||____| | | ||____|  |____|| | | |________.'  | | |   `.____.'   | | |  |__/  \\__|  | |\n");
        printf("| |              | | |              | | |              | | |              | | |              | | |              | |\n");
        printf("| '--------------' | '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |\n");
        printf(" '----------------' '----------------' '----------------' '----------------' '----------------' '----------------' \n");
        printf(" .----------------. .----------------. .----------------. .----------------. .-----------------.\n");
        printf("| .--------------. | .--------------. | .--------------. | .--------------. | .--------------. |\n");
        printf("| |  _________   | | |     _____    | | |  _________   | | |      __      | | | ____  _____  | |\n");
        printf("| | |  _   _  |  | | |    |_   _|   | | | |  _   _  |  | | |     /  \\     | | ||_   \\|_   _| | |\n");
        printf("| | |_/ | | \\_|  | | |      | |     | | | |_/ | | \\_|  | | |    / /\\ \\    | | |  |   \\ | |   | |\n");
        printf("| |     | |      | | |      | |     | | |     | |      | | |   / ____ \\   | | |  | |\\ \\| |   | |\n");
        printf("| |    _| |_     | | |     _| |_    | | |    _| |_     | | | _/ /    \\ \\_ | | | _| |_\\   |_  | |\n");
        printf("| |   |_____|    | | |    |_____|   | | |   |_____|    | | ||____|  |____|| | ||_____|\\____| | |\n");
        printf("| |              | | |              | | |              | | |              | | |              | |\n");
        printf("| '--------------' | '--------------' | '--------------' | '--------------' | '--------------' |\n");
        printf(" '----------------' '----------------' '----------------' '----------------' '----------------' \n");

        printf("\n\n ENTER PARA INICIAR O JOGO\n\n");
        getche();
        GAME_STATE = GAME;
    }
    if(GAME_STATE == MAIN_MENU){
        //show main menu
    }
    //if(GAME_STATE == SETTINGS){
    //    //show settings
    //}
    if(GAME_STATE == PRE_GAME) {
        //load game stuff
    }
    if(GAME_STATE == GAME){
        if(start() == OK){
        bool quit = false;
        while(quit == false){ //repensar este loop
            if(checkEvents() == QUIT){
                quit = true;
            }
            if(quit == false){
                update();
                resolveDirecaoHerois();
                draw();
            }
        }
    }

    close();
    }
    if(GAME_STATE == PRE_ROUND){
        // load round stuff
    }
    if(GAME_STATE == ROUND){
        // do round stuff
    }
    if(GAME_STATE == POS_ROUND){
        // do pos round
    }
    if(GAME_STATE == GAMEOVER){
        // do gameover stuff
    }
    if(GAME_STATE == CREDITS){
        // show credits
    }
    return 0;
}
